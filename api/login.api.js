require('dotenv').config()

const router = require('express').Router()
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')


const jsonParser = bodyParser.json()

router.post('/login', jsonParser, (req, res) => {
  console.log(req.body.name);
  const name = req.body.name

  const accessToken = jwt.sign(name, process.env.ACCESS_TOKEN_SECRET)

  res.json({ name: name, accessToken: accessToken })
})

module.exports = router