const allUsers = new Map()
const rooms = [ 
  { name: 'General' },
  { name: 'Java' }
]

function addUser({ id, name }) {
  const user = { name }

  if (allUsers.has(id)) return { error: 'Already in room'}

  allUsers.set(id, user)

  return { user }
}

function getUser(id) {
  return allUsers.get(id)
}

function getAllUsers() {
  const users = []
  allUsers.forEach(user => {
    users.push(user)
  })

  return users
}

function removeUser(id) {
  if (allUsers.has(id)) {
    const user = allUsers.get(id)
    allUsers.delete(id)
    return user
  }
}



function getRoom(inputRoom) {
  return rooms.find(room => room.name === inputRoom)
}

function getAllRooms() {
  return rooms.map(room => room.name)
}


module.exports = {
  addUser,
  getUser,
  getAllUsers,
  removeUser,
  getAllRooms,
}
