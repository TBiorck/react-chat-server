# React Chat Server

This is the Node Express server for the React Chat task.

## Installation

```bash
npm install
```

## Usage
Start the server before running the React client application.

```bash
npm start
```

