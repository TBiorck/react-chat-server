require('dotenv').config()

const app = require('express')()
const server = require('http').createServer(app)
const io = require('socket.io')(server)
const jwt = require('jsonwebtoken')
const cors = require('cors')

const chatManager = require('./chat/chat-manager.js')
const loginApi = require('./api/login.api.js')

const PORT = process.env.PORT || 3001

// Setup CORS middleware
app.use(cors())


// Setup socket.io
io.use((socket, next) => {
  if (socket.handshake.query && socket.handshake.query.token) {
    jwt.verify(socket.handshake.query.token, process.env.ACCESS_TOKEN_SECRET, (err, decoded) => {
      if (err) {
        return next(new Error('Authentication error'))
      }
      socket.decoded = decoded
      next()
    })
  }
  else {
    next(new Error('Authentication error'))
  }
}).on('connection', socket => {

  // On user join room
  socket.on('join', ({ name }, callback) => {
    const { error, user } = chatManager.addUser({ id: socket.id, name })

    console.log('allusers:', chatManager.getAllUsers())

    if (error) return callback(error)

    socket.join('allUsers')

    io.to('allUsers').emit('usersData', { users: chatManager.getAllUsers() })
    io.to(socket.id).emit('roomsData', { rooms: chatManager.getAllRooms() })

    callback()
  })

  socket.on('joinRoom', ({ name, room }, callback) => {
    socket.emit('message', { user: 'Server', text: `Hi, ${name}! Welcome to the room ${room}` })
    socket.broadcast.to(room).emit('message', { user: 'Server', text: `${name} has joined the chat!`})
    
    const storedUser = chatManager.getUser(socket.id)
    if (!storedUser) {
      chatManager.addUser({ id: socket.id, name })
    }
    chatManager.getUser(socket.id)['room'] = room
    
    socket.join(room)
  })

  // User send message
  socket.on('sendMessage', ({ message, room }, callback) => {
    const user = chatManager.getUser(socket.id)
    console.log('HEYA', user)

    io.to(room).emit('message', { user: user.name, text: message})

    callback()
  })

  // On user disconnect
  socket.on('disconnect', () => {
    console.log('disonnected', socket.id)
    const user = chatManager.removeUser(socket.id)

    if (user) {
      io.to('allUsers').emit('usersData', { users: chatManager.getAllUsers() })

      console.log(`Removed ${user.name}`);
      console.log('allUsers are now:::', chatManager.getAllUsers())

      if (user.room) {
        io.to(user.room).emit('message', { user: 'Server', text: `${user.name} has left the chat.`})
      }
    }
  })
})

// Setup API middleware
app.use(loginApi)

// Start server
server.listen(PORT, () => console.log(`Server is listening on port ${PORT}`))